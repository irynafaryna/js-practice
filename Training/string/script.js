// Перехід стрічки
//alert(`
//   Ірина
//   Фарина
//   Несторівна`); 

// Введення через ${}
//'use strict'; строгий режим
// let apples = 2;
// let oranges = 3;
// alert(`${apples} + ${oranges} = ${apples + oranges}`); // 2 + 3 = 5


// 'use strict';

// function f(strings, ...values) {
//   alert(JSON.stringify(strings));     // ["Sum of "," + "," =\n ","!"]
//   alert(JSON.stringify(strings.raw)); // ["Sum of "," + "," =\\n ","!"]
//   alert(JSON.stringify(values));      // [3,5,8]
// }

// let apples = 3;
// let oranges = 5;

// //          |  s[0] | v[0] |s[1]| v[1]  |s[2]  |      v[2]      |s[3]
// let str = f`Sum of ${apples} + ${oranges} =\n ${apples + oranges}!`;

//eval(тут стрічка) обраховує значення
// var v="10+15";
// console.log(eval(v1));

// trim() видаляє пробіли спочатку і вкінці
// var V = '   gff  f    '
// console.log(V.trim());

// Пошук довжини стрічки
// let browserType = 'mozilla';
// browserType.length;

// Отримати певний символ стрічки
// browserType[0];

// Отримати останній символ
// browserType[browserType.length-1];

// Пошук підстрічки всередині основної стрічки
// browserType.indexOf('zilla');
// виведеться позиція з якої починається підстрічка

// Зміна регістра
// var radData = 'My NaMe Is MuD';
// radData.toLowerCase();
// radData.toUpperCase();

// Зміна частини стрічки на іншу
// let browserType = 'mozilla';
// console.log(browserType.replace('moz','van'));

document.querySelector('button').onclick = myClick;
function myClick() {
  let a = document.querySelector('.symbol-input').value;
  document.querySelector('.symbol').innerHTML = a.toUpperCase();

}
