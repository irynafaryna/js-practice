const data = [
  {
    name: 'Irynka',
    age: 18,
    english: true,
    skils: ['HTML', 'CSS', 'JS'],
    boy: {
      exist: true,
      info: {
        name: 'Yuriy',
        education: ['school', 'University']
      }
    }
  },
  {
    name: 'Tania',
    age: 18,
    english: true,
    skils: ['Pravo'],
    boy: {
      exist: false,
      info: {}
    }
  },
  {
    name: 'Oksana',
    age: 27,
    english: false,
    skils: ['Dance', 'Cooking'],
    boy: {
      exist: true,
      info: {
        name: 'Vasyliy',
        education: ['school', 'University']
      }
    }
  },
  {
    name: 'Sophiya',
    age: 19,
    english: true,
    skils: ['JS', 'C++', 'Java'],
    boy: {
      exist: false,
      info: {}
    }
  },
  {
    name: 'Yulia',
    age: 21,
    english: true,
    skils: ['Dance', 'Sing'],
    boy: {
      exist: true,
      info: {
        name: 'Andriy',
        education: ['logos', 'academy']
      }
    }
  }
]

// вивсести масив з людей в яких є хлопець; getPeopleWithBoy();+
// збільшити вік кожного на 1 рік;+
// вивести всі види освіт які є в людей;+
// добавити кожному ключ "city" і вказати в значені того ключа місто в якому вони проживають;
// Виведи імена хлопців, якщо вони є;
// Виввести список людей, які мають скіли "Dance"; +
// Вивести людину, яка має найбільше скілів;
// посортувати людей за віком від старшого до молодшого;+

function getPeopleWithBoy(arr) {
  let peopleWithBoy = arr.filter((people) => people.boy.exist)
  console.log('getPeopleWithBoy', peopleWithBoy);
}
getPeopleWithBoy(data)

function getPeopleWithoutBoy(arr) {
  let peopleWithoutBoy = arr.filter((people) => !people.boy.exist)
  console.log('getPeopleWithoutBoy', peopleWithoutBoy);
}
getPeopleWithoutBoy(data)

function increaseAge(arr) {
  let peopleWithNewAge = arr.map((people) => {
    people.age += 1;
    return people
  })
  console.log('increaseAge', peopleWithNewAge);
}
increaseAge(data)

function getPeopleWhoLogos(arr) {
  let peopleWithLogos = arr.find(function (people) {
      if (people.boy.exist) {
          for (let i = 0; i < people.boy.info.education.length; i++) {
              return people.boy.info.education[i] === 'logos';
          }
      }
      else return false
  })
  console.log('getPeopleWhoLogos', peopleWithLogos);
}
getPeopleWhoLogos(data)

function getPeopleWhoDance(arr) {
  let peopleWhoDance = data.filter(function(people){
    for (let j = 0; j < people.skils.length; j++) {
      return people.skils[j] === 'Dance';
    }
  })
  console.log('getPeopleWhoDance', peopleWhoDance); 
}
getPeopleWhoDance(data)

function getBoysEducation() {
  let educations = [];
  data.map((people) => {
    if (people.boy.exist) {
      people.boy.info.education.forEach((educat) => {
        if (!educations.includes(educat)) {
          educations.push(educat)
        }
      })
    }
  });
  console.log('getBoysEducation', educations);
}
getBoysEducation(data)


function getPepleWithMostSkils(arr){
  let skilsCount = []
  arr.map((people) => skilsCount.push(people.skils.length))

  const peopleWithMostSkils = arr.filter((people) => people.skils.length === Math.max(...skilsCount))
  console.log('getPepleWithMostSkils', peopleWithMostSkils)
}
getPepleWithMostSkils(data)

function showPeopleWithAddedCity(arr){
  let peopleWithAddedCity = arr.map((people) => {
    people.city = "Lviv";
    return people
  })
  console.log('showPeopleWithAddedCity', peopleWithAddedCity);
}
showPeopleWithAddedCity(data)

function getBoysName(arr) {
  let boysName = [];
  data.map((people) => {
    if (people.boy.exist) {
      boysName.push(people.boy.info.name)
    }
  });
  console.log('getBoysName', boysName);
}
getBoysName(data)


function dataSort(arr) {
  let datasort = data.sort(function (a,b){
    return b.age - a.age;
  })
  console.log('dataSort', datasort);
}
dataSort(data)